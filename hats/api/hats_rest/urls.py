from django.urls import path
from .views import list_of_hats, detail_of_hats


urlpatterns = [
    path("hats/", list_of_hats, name="list_hats"),
    path('hats/<int:pk>/', detail_of_hats, name="detail_hats"),
]


