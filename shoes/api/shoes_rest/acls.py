import json
import requests

from .keys import PEXELS_API_KEY


def get_photo(shoes_name, manufacturer_name, color):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": f"shoes {shoes_name} {color} {manufacturer_name}"}
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"shoes_picture": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"shoes_picture": None}
