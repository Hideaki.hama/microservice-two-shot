from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import BinVO, Shoes
from .acls import get_photo

# from wardrobe.api.wardrobe_api.models import Bin


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href", "closet_name", "bin_number", "bin_size"]


class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "shoes_picture",
        "shoes_name",
        "manufacturer_name",
        "color",
        "id",
        "bin",
    ]
    encoders = {"bin": BinVOEncoder()}


class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = ["shoes_picture", "shoes_name", "manufacturer_name", "color", "bin"]
    encoders = {"bin": BinVOEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse({"shoes": shoes}, encoder=ShoesListEncoder, safe=False)
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Bin doesn't exist"},
                status=400,
            )

        photo = get_photo(
            content["shoes_name"], content["manufacturer_name"], content["color"]
        )
        content.update(photo)

        shoes = Shoes.objects.create(**content)
        return JsonResponse(shoes, encoder=ShoesDetailEncoder, safe=False)


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_shoes(request, pk):
    if request.method == "GET":
        shoes = Shoes.objects.get(id=pk)
        return JsonResponse(shoes, encoder=ShoesDetailEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=pk).delete()
        return JsonResponse({"deleted?": count > 0})
    else:
        content = json.loads(request.body)
        Shoes.objects.filter(id=pk).update(**content)
        shoes = Shoes.objects.get(id=pk)
        return JsonResponse(shoes, encoder=ShoesDetailEncoder, safe=False)
