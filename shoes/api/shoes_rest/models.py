from django.db import models


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, blank=True, null=True, unique=True)
    closet_name = models.CharField(max_length=200)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()

    def __str__(self):
        return f"{self.closet_name} - {self.bin_number}/{self.bin_size}"


class Shoes(models.Model):
    shoes_name = models.CharField(max_length=200)
    manufacturer_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    shoes_picture = models.URLField()
    bin = models.ForeignKey(
        BinVO, related_name="shoes", on_delete=models.CASCADE, null=True
    )
