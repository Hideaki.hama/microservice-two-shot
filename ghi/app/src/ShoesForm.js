import React from 'react';
import { NavLink } from 'react-router-dom';

class ShoesForm extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      shoesName:'',
      color:'',
      manufacturerName:'',
      shoesPicture:'',
      bin:'',
      bins:[]
    };

    this.handleShoesNameChange= this.handleShoesNameChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
    this.handleShoesPictureChange = this.handleShoesPictureChange.bind(this);
    this.handleBinChange = this.handleBinChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    }


    async handleSubmit(event) {
      event.preventDefault();
      const data = {...this.state};
      data.shoes_name = data.shoesName;
      data.manufacturer_name = data.manufacturerName
      data.shoes_picture = data.shoesPicture


      delete data.shoesName;
      delete data.manufacturerName;
      delete data.shoesPicture;
      delete data.bins;

      const shoesUrl = 'http://localhost:8080/api/shoes/';
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(shoesUrl, fetchConfig);
      if (response.ok) {
        const newShoes = await response.json()
        console.log(newShoes)


        this.setState({
          shoesName:'',
          color:'',
          manufacturerName:'',
          shoesPicture:'',
          bin:'',
        });
      }
      window.location.reload();
    }

    handleShoesNameChange(event){
      const value = event.target.value;
      this.setState({shoesName:value})
    }

    handleColorChange(event){
      const value = event.target.value;
      this.setState({color:value})
    }

    handleManufacturerChange(event){
      const value = event.target.value;
      this.setState({manufacturerName:value})
    }

    handleShoesPictureChange(event){
      const value = event.target.value;
      this.setState({shoesPicture:value})
    }

    handleBinChange(event){
      const value = event.target.value;
      this.setState({bin:value})
    }





  async componentDidMount() {
    const url = 'http://localhost:8100/api/bins/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

        this.setState({bins: data.bins});

    }
  }
  render() {
    return(
<div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new shoes</h1>
              <form onSubmit={this.handleSubmit} id="create-shoes-form">
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleShoesNameChange}
                    value = {this.state.shoesName}
                    placeholder="Shoes Name"
                    required
                    type="text"
                    name="shoes_name"
                    id="shoes_name"
                    className="form-control"
                  />
                  <label htmlFor="name">Shoes Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleColorChange}
                    value = {this.state.color}
                    placeholder="Color"
                    required
                    type="text"
                    id="color"
                    name="color"
                    className="form-control"
                  />
                  <label htmlFor="room_count">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleManufacturerChange}
                    value = {this.state.manufacturerName}
                    placeholder="Manufacturer name"
                    required
                    type="text"
                    id="manufacturer_name"
                    name="manufacturer_name"
                    className="form-control"
                  />
                  <label htmlFor="city">Manufacturer's name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleShoesPictureChange}
                    value = {this.state.shoesPicture}
                    placeholder="Picture URL"
                    required
                    type="text"
                    id="shoes_picture"
                    name="shoes_picture"
                    className="form-control"
                  />
                  <label htmlFor="city">Picture URL</label>
                </div>
                <div className="mb-3">
                <select required value={this.state.bin} onChange={this.handleBinChange} id="bin" name="bin" className="form-select">
                    <option value="" >Choose the Closet Name with Bin # and Size</option>
                    {this.state.bins.map(bin => {
                      return (
                        <option key={bin.href} value={bin.href}>
                             {bin.closet_name} - {bin.bin_number}/{bin.bin_size}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <p>Can't find a right bin? <NavLink className="h-0" to="/shoes/bin">Click here!</NavLink></p>
                <button className="btn btn-primary">Create</button>
                <NavLink to="/shoes/list" className="btn btn-primary" >Back</NavLink>
              </form>
            </div>
          </div>
        </div>
    );
  }
}

export default ShoesForm;
