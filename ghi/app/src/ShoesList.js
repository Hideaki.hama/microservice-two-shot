import { NavLink } from 'react-router-dom';

const deleteShoes = async (id) => {

  fetch(`http://localhost:8080/api/shoes/${id}/`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  window.location.reload();
}



function ShoesList(props) {

    return(
    <div>

        <NavLink className="btn btn-primary btn-lg px-4 gap-3" to="/shoes/new">Create</NavLink>

      <table className="table table-striped">
        <thead>
          <tr>
            <th></th>
            <th>Name</th>
            <th>Color</th>
            <th>Manufacturer's Name</th>
            <th>Closet Name</th>
            <th>Bin Number</th>
            <th>Bin Size</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes.map(shoes => {
            return (
              <tr key={shoes.id}>
                <td><img src={shoes.shoes_picture} width="50" height="50" className="" /></td>
                <td>{shoes.shoes_name}</td>
                <td>{shoes.color}</td>
                <td>{shoes.manufacturer_name}</td>
                <td>{shoes.bin["closet_name"]}</td>
                <td>{shoes.bin["bin_number"]}</td>
                <td>{shoes.bin["bin_size"]}</td>

                <td><button className="btn btn-secondary" onClick={() => deleteShoes(shoes.id)} type="button">Delete</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ShoesList;