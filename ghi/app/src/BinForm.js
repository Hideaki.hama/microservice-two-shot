import React from 'react';
import { Navigate } from 'react-router';


class BinForm extends React.Component{


  constructor(props) {
    super(props)

    this.state ={
      closetName:'',
      binNumber:'',
      binSize:'',

    };


    this.handleClosetNameChange = this.handleClosetNameChange.bind(this);
    this.handleBinNumberChnage = this.handleBinNumberChnage.bind(this);
    this.handleBinSizeChange = this.handleBinSizeChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this)
  }




  async handleSubmit(event) {
    event.preventDefault();


    const data = {...this.state}
    data.closet_name = data.closetName;
    data.bin_number = data.binNumber;
    data.bin_size = data.binSize;

    delete data.closetName;
    delete data.binNumber;
    delete data.binSize;

    const binUrl = "http://localhost:8100/api/bins/"
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      header: {
        'Content-Type': 'appication/json'
      }
    }
    const response = await fetch(binUrl, fetchConfig);
    if(response.ok) {
      const newBin = await response.json()
      console.log(newBin)

      this.setState ({
        closetName:'',
        binNumber:'',
        binSize:'',
        user:true
      });

    }

  }

    handleClosetNameChange(event){
      const value = event.target.value;
      this.setState({closetName:value})
    }

    handleBinNumberChnage(event){
      const value = event.target.value;
      this.setState({binNumber:value})
    }

    handleBinSizeChange(event){
      const value = event.target.value;
      this.setState({binSize:value})
    }

  render() {
    let { user } = this.state;
    return(
<div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new bin</h1>
              {user && (<Navigate to="/shoes/new"/>)}
              <form onSubmit={this.handleSubmit} id="create-bin-form">
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleClosetNameChange}
                    value = {this.state.closetName}
                    placeholder="Closet Name"
                    required
                    type="text"
                    name="closet_name"
                    id="closet_name"
                    className="form-control"
                  />
                  <label htmlFor="name">Closet Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleBinNumberChnage}
                    value = {this.state.binNumber}
                    placeholder="Bin Number"
                    required
                    type="text"
                    id="bin_number"
                    name="bin_number"
                    className="form-control"
                  />
                  <label htmlFor="room_count">Bin Number</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleBinSizeChange}
                    value = {this.state.binSize}
                    placeholder="Bin Size"
                    required
                    type="text"
                    id="bin_size"
                    name="bin_size"
                    className="form-control"
                  />
                  <label htmlFor="city">Bin Size</label>
                </div>


                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
    )
  }


}

export default BinForm;
