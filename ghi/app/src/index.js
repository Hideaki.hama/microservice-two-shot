import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './index.css';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


// reportWebVitals();

async function loadShoes() {
  const response = await fetch('http://localhost:8080/api/shoes/');
  if (response.ok) {
    const data = await response.json();
    root.render(
      <React.StrictMode>
        <App shoes={data.shoes} />
      </React.StrictMode>
    );
  } else {
    console.error(response);
  }
}
loadShoes();

async function loadHats(){
  const hatsResponse = await fetch('http:..localhost/api/hats');
  if(hatsResponse.ok){
    const hatData = await hatsResponse.json();
    root.render(
    <React.StrictMode>
      <App hats={hatData} />
    </React.StrictMode>
    );
  } else {
    console.error(hatsResponse);
  }
}
loadHats()
