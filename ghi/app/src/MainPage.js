
import React from 'react';
import { Link } from 'react-router-dom';

function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Need to keep track of your shoes and hats? We have
          the solution for you!
        </p>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Add new shoes</Link>
      </div>
    </div>
      </div>
  );
}

export default MainPage;











// import React from 'react';
// import { Link } from 'react-router-dom';

// function ShoesColumn(props) {
//   return (
//     <div className="col">
//       {props.list.map(data => {
//         const shoes = data.shoes;
//         return (
//           <div key={shoes.href} className="card mb-3 shadow">
//             {/* <img src={shoes.shoes_picture} className="card-img-top" /> */}
//             <div className="card-body">
//               <h5 className="card-title">{shoes.shoes_name}</h5>
//               <h6 className="card-subtitle mb-2 text-muted">
//                 {shoes.manufacturer}
//               </h6>
//               <p className="card-text">
//                 {shoes.color}
//               </p>
//             </div>
//             <div className="card-footer">
//               {/* {new Date(conference.starts).toLocaleDateString()}
//               -
//               {new Date(conference.ends).toLocaleDateString()} */}
//             </div>
//           </div>
//         );
//       })}
//     </div>
//   );
// }

// class MainPage extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       shoesColumns: [[], [], []],
//     };
//   }

//   async componentDidMount() {
//     const url = 'http://localhost:8080/api/shoes/';

//     try {
//       const response = await fetch(url);
//       if (response.ok) {
//         // Get the list of conferences
//         const data = await response.json();

//         // Create a list of for all the requests and
//         // add all of the requests to it
//         const requests = [];
//         for (let shoes of data.shoes) {
//           const detailUrl = `http://localhost:8080/api/shoes/${shoes.id}`;
//           requests.push(fetch(detailUrl));
//         }

//         // Wait for all of the requests to finish
//         // simultaneously
//         const responses = await Promise.all(requests);

//         // Set up the "columns" to put the conference
//         // information into
//         const shoesColumns = [[], [], []];

//         // Loop over the conference detail responses and add
//         // each to to the proper "column" if the response is
//         // ok
//         let i = 0;
//         for (const shoesResponse of responses) {
//           if (shoesResponse.ok) {
//             const details = await shoesResponse.json();
//             shoesColumns[i].push(details);
//             i = i + 1;
//             if (i > 2) {
//               i = 0;
//             }
//           } else {
//             console.error(shoesResponse);
//           }
//         }

//         // Set the state to the new list of three lists of
//         // conferences
//         this.setState({shoesColumns: shoesColumns});
//       }
//     } catch (e) {
//       console.error(e);
//     }
//   }




//   render(){
//     return (
//     <div className="px-4 py-5 my-5 text-center">
//       <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
//       <div className="col-lg-6 mx-auto">
//         <p className="lead mb-4">
//           Need to keep track of your shoes and hats? We have
//           the solution for you!
//         </p>
//         <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
//               <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Add new shoes</Link>
//         </div>
//       </div>
//       <div className="container">
//           <h2>Shoes collections</h2>
//           <div className="row">
//             {this.state.shoesColumns.map((shoesList, index) => {
//               return (
//                 <ShoesColumn key={index} list={shoesList} />
//               );
//             })}
//           </div>
//       </div>
//     </div>
//     );
//   }
// }