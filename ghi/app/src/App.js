import { BrowserRouter, Routes, Route, useNavigate } from 'react-router-dom';
import MainPage from './MainPage';
import ShoesForm from './ShoesForm';
import ShoesList from './ShoesList';
import BinForm from './BinForm'
import Nav from './Nav';
import HatsList from './HatsList'
import HatForm from './HatForm'



function App(props) {
  if (props.shoes === undefined){
    return null;
  }



  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route  path="list" element={<ShoesList shoes={props.shoes} />} />
            <Route path="new" element={<ShoesForm />} />
            <Route path="bin" element={<BinForm />} />
          </Route>
          <Route path="/" element={<MainPage/>} />
          <Route path="/hats/list" element={<HatsList/>} />
          <Route path="/hats/new" element={<HatForm/>}/>
        </Routes>
    </BrowserRouter>
  );
}

export default App;
