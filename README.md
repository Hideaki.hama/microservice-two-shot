# Wardrobify

Team:

* Person 1 - Which microservice?
* Person 2 - Which microservice?

## Design

Shoes Form with:
  1)Picture of the shoes   shoes_picture
  2)Shoes name  shoes_name
  3)Color  color
  4)Manufacturer's name  manufacturer_name
  5)Bin-Closet Name

## Port Information ##
    Hats: port 8090
    Shoes: port 8080
    Wardrobe(for browser or insomnia): port 8100
    Wardrobe(for polling service): port 8000
        base URL of poll -> http://wardrobe-api:8000
    React: port 3000

## URLs Information ##
    Bin Urls
      -GET- 	gets a list of all of the bins   http://localhost:8100/api/bins/
      -GET-   gets the details of one bin   http://localhost:8100/api/bins/<int:pk>/
      -DEL-   deletes a single bin  http://localhost:8100/api/bins/<int:pk>/
      -PUT-   updates the details of one bin  http://localhost:8100/api/bins/<int:pk>/
      -POST-  creates a new bin with the posted data  http://localhost:8100/api/bins/

    Two RESTful views with urls:
    api_list_shoes functins for:
      -GET-  get the list of shoes  http://localhost:8080/api/shoes/
      -POST- Create a shoes   http://localhost:8080/api/shoes/
    pi_show_shoes function for:
      -GET-  get the detail of shoes http://localhost:8080/api/shoes/<int:pk>/
      -PUT-  Update a shoes  http://localhost:8080/api/shoes/<int:pk>/
      -DELETE-  Delete a shoes  http://localhost:8080/api/shoes/<int:pk>/

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

Bin model from wardrobe api has following properties
-closet_name-
-bin_number-
-bin_size-

Two Models:
  Shoes
    has shoes_name, manufacturer_name, color, shoes_picture, bin (as a foreignKey)
  BinVO
    is a Valu Object from wardrobe's bin model.  For just 'get'ing the data.

Two RESTful views with urls:
  api_list_shoes functins for:
    -GET-  get the list of shoes  http://localhost:8080/api/shoes/
    -POST- Create a shoes   http://localhost:8080/api/shoes/
  api_show_shoes function for:
    -GET-  get the detail of shoes http://localhost:8080/api/shoes/<int:pk>/
    -PUT-  Update a shoes  http://localhost:8080/api/shoes/<int:pk>/
    -DELETE-  Delete a shoes  http://localhost:8080/api/shoes/<int:pk>/

  ***
  models.py of wardrobe.api following was changed:
  line 26 api_location to api_bin  (Typo?)

  ***
  settings.py following was added:
  'django_crontab' added into the INSTALLED_APPS
  CRONJOBS = [("* * * * *", "shoes.poll.poll.get_bins")]

  ***
  django-crontab==0.7.1 was added to bot requirements.txt in the pull & api folder of shoes.

  ***
  acls.py or Anti Corruption Layers
  contain pexels search api.  Will be using to get randomly generated shows by shoes name (doesnt work always since its randomly searched)


## Hats microservice

1 - hats/api is not in django so I put the app into INSTALLED APPS as hats_rest.apps.HatsApiConfig.
2 - Where are no urls, views or models
3 - Added path('api/', include('hats_rest.urls)) to the urlpatterns
4 - created git branch named hat
5 - Created Hats model that contains
        - fabric
        - stylename
        - color
        - url for picture using a models.URLField
        - ForeignKey to Locations (within the wardrobe)
6 - Create RESTful Api to get
        - List
        - Create a new hat
        - Delete a new Hat
7 - Create REACT components to
        - Show list of hats and their details
        - Show a form to create a new hat
8 - Provide a way to delete a hat
9 - Provide a existing nav link to your components